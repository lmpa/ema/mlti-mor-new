function Im=DiamPD(V,W,m,K2)
for i=1:m
    i1=(i-1)*K2+1:i*K2;
    for j=1:m
        j1=(j-1)*K2+1:j*K2;
        Im(i,j)=innerprod(tensor(V(:,:,:,i1)),tensor(W(:,:,:,j1)));
    end 
end

end

