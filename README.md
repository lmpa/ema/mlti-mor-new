This MATLAB program focuses on Model Order Reduction for Multilinear Time-Invariant (MLTI) systems, employing projection techniques through both tensor classic and extended Krylov subspaces.
  
  
THIS SOFTWARE IS BEING PROVIDED "AS IS", WITHOUT ANY EXPRESS OR IMPLIED WARRANTY.                                                           ANY USE OF THE SOFTWARE CONSTITUTES  ACCEPTANCE OF THE TERMS OF THE ABOVE STATEMENT.

# Authors 
                                           
     M. A. HAMIDI                         
     Université du Littoral, Calais, France
     E-mail: mohamed-amine.hamadi@centralesupelec.fr
     
     K. JBILOU                                                            
     Université du Littoral, Calais, France                               
     E-mail: khalide.jbilou@univ-littoral.fr                                

     A. RATNANI                                                            
     Université polytechnique Mohammed VI, Maroc                             
     