%This script demonstrates the process of obtaining 
%a reduced MLTI system using the tensor extended global Krylov subspace

% Authors : Mohamed Amine HAMADI & Khalide Jbilou & Ahmed Ratnani
% contact : mohamed-amine.hamadi@centralesupelec.fr

%more details could be found in :
%A model reduction method for large-scale linear multidimensional dynamical systems
%https://arxiv.org/abs/2305.09361
clear all
clc
% rng(10),
K1=5; K2=5;
N=128;
nBC=K1*K2;

%%%   Tensors %%%
a=-1-0.5; b=-8; c=-1+0.5;
A=spdiags([a*ones(N^2,1), b*ones(N^2,1), c*ones(N^2,1)],-1:1,N^2,N^2);
Aten=spreshape(tensor(A),[N,N,N,N]);
Bten=sptenrand([N,N,K1,K2],N*N);Bten=tensor(Bten);
Cten=tenrand([K1,K2,N,N]);

%LU decomposition
[L,U]=lu(A);

%%B and C Matrices
B=reshape(double(Bten),[N*N,K1*K2]); C=reshape(double(Cten),[K1*K2,N*N]);

% Dimension of the tensor extended Krylov sub.
m=5;

tic
[V,H,o11,T]=Tensor_extended_global_process(Aten,L,U,Bten,B,m);
tfinal=toc;
fprintf('Time consumed to get the associated basis tensor V_m: %f\n', tfinal);

Vm=V(:,:,:,1:2*m*K2);
Im=eye(2*m); em=Im(:,1);
I4=Identity4(K1,K2,K1,K2);
freq=linspace(0,2*pi,50);

%%% Structure tensoriel of the Reduced model %%%
Ctenm=ttt(Cten,Vm,[3,4],[1,2]);
Btenm=KronTens(o11*em,I4);
Atenm=KronTens(full(T(1:2*m,1:2*m)),I4);


Amatm=reshape(Atenm,[2*m*nBC,2*m*nBC]);
Bmatm=reshape(Btenm,[2*m*nBC,nBC]);
Cmatm=reshape(double(Ctenm),[nBC,2*m*nBC]);
% % 
% 
% % 
tfFm=lp_trfia_mod(freq,Amatm,Bmatm,Cmatm,[],[]);
tfF=lp_trfia_mod(freq,A,B,C,[],[]);
nrm_tfF=lp_gnorm(tfF,nBC,nBC);
nrm_tfFm=lp_gnorm(tfFm,nBC,nBC);
% Plotting the figures
semilogy(freq,nrm_tfF,'red')
hold on
semilogy(freq,nrm_tfFm,'o')
xlabel('Frequency (rad/sec)')
ylabel('Magnitude')
title('Norm of frequency responses')
figure()
err=lp_gnorm(tfF-tfFm,nBC,nBC);
semilogy(freq,err)
xlabel('Frequency (rad/sec)')
ylabel('Absolute error')
title('Closeness of the two transfer functions')