function [V,H,o11,T] = Tensor_extended_global_process(A,L1,U1,B,Bmt,m)
%EXT_GLO_TEN_NEW Summary of this function goes here
%   Detailed explanation goes here
a=size(B);
J1=a(1); J2=a(2); K1=a(3);K2=a(4);
% [J1,J2,K1,K2]=size(B);
I4=Identity4(K1,K2,K1,K2);
H=sparse(2*(m+1),2*m);
T=sparse(2*(m+1),2*m);
K22=2*K2;
V=tensor(zeros(J1,J2,K1,(m+1)*K22));
Vold=tensor(zeros(J1,J2,K1,K22)); 
%%%%%%%%%%%%%%
% VV=Amt\Bmt;
VV=U1\(L1\Bmt);
VV=reshape(VV,[J1,J2,K1,K2]);
%%%%%%%%%%%%%%
Vold(:,:,:,K2+1:K22)=VV;
Vold(:,:,:,1:K2)=B;
[Vj,Omega]=global_ortho(Vold,K2);
o11=Omega(1,1); o22inv=1/Omega(2,2); o12=Omega(1,2);
V(:,:,:,1:K22)=Vj;
k=1;
for j=1:m
    W=tensor(zeros(J1,J2,K1,K22));
    jj=2*j-1:2*j;
    %W=ttt(tensor(A),tensor(Vold),[3,4],[1,2]);
    %j1=(j-1)*K2+1:j*K2; j2=j*K2+1:(j+1)*K2;
    j1=1:K2; j2=K2+1:K22;
    W(:,:,:,j1)=ttt(A,Vj(:,:,:,j1),[3,4],[1,2]);
    matVj2=reshape(Vj(:,:,:,j2),[J1*J2,K1*K2]);
%     VV=Amt\double(matVj2);
    VV=U1\(L1\double(matVj2));
    W(:,:,:,j2)=reshape(VV,[J1,J2,K1,K2]);
    %[W(:,:,:,j2),~]=Gl_GMRES_mod(A,Vj(:,:,:,j2),X0,restrt,max_it,K2);
    for i=1:j
        ii=2*i-1:2*i;
        i1=(i-1)*K22+1:i*K22;
        H(ii,jj)=DiamPD(V(:,:,:,i1),W,2,K2);
        dd=tensor(KronTens(full(H(ii,jj)),I4));
        W=W-ttt(V(:,:,:,i1),dd,[3,4],[1,2]);
    end
    [Vj,H(jj+2,jj)]=global_ortho(W,K2);
    %size(Vj),
    jnew=j*K22+1:(j+1)*K22;
    V(:,:,:,jnew)=Vj;
    %%%%%%%%% Computing Tj %%%%%%%%%%%
    Ijj=speye(2*(j+1));
    if (j==1)
    T(1:4,1)=H(1:4,1);
    T(1:4,2)=o22inv*(o11*Ijj(:,1)-o12*H(1:4,1));
    else
    Ijjk=speye(2*(k+1));
    T(1:2*(j+1),2*j-1)=H(1:2*(j+1),2*j-1);
    aa=H(2*k+2,2*k); aa1=1/aa;
    T(1:2*(j+1),2*k+2)= aa1*(Ijj(:,2*k)-T(1:2*(j+1),1:2*k+1)*H(1:2*k+1,2*k));
    k=k+1;
    end
end
end

